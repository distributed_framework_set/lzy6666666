import{_ as i,o as n,c as e,e as s}from"./app-9c8f6567.js";const l="/lzy6666666/images/es-1.png",d="/lzy6666666/images/es-2.png",v={},u=s(`<h1 id="elasticsearch" tabindex="-1"><a class="header-anchor" href="#elasticsearch" aria-hidden="true">#</a> Elasticsearch</h1><ol><li><p>概念</p><ul><li><p>Index ：类似于mysql数据库中的database</p></li><li><p>Type： 类似于mysql数据库中的table表，es中可以在Index中建立type（table），通过mapping进行映射。6.0版本不再支持Type，固定doc，7.0直接去掉了type,包括API，默认加上doc，8.0则彻底去掉type。</p></li><li><p>Document：由于es存储的数据是文档型的，一条数据对应一篇文档即相当于mysql数据库中的一行数据row，一个文档中可以有多个字段也就是mysql数据库一行可以有多列。</p></li><li><p>Field：es中一个文档中对应的多个列与mysql数据库中每一列对应</p></li><li><p>Mapping：可以理解为mysql或者solr中对应的schema，只不过有些时候es中的mapping增加了动态识别功能，感觉很强大的样子，其实实际生产环境上不建议使用，最好还是开始制定好了对应的schema为主。</p></li><li><p>indexed：就是名义上的建立索引。mysql中一般会对经常使用的列增加相应的索引用于提高查询速度，而在es中默认都是会加上索引的，除非你特殊制定不建立索引只是进行存储用于展示，这个需要看你具体的需求和业务进行设定了。</p></li><li><p>Query DSL：类似于mysql的sql语句，只不过在es中是使用的json格式的查询语句</p></li></ul></li><li><p>CRUD</p><p>默认分页大小10，从0开始。</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code># 8.5中的使用

####

# 创建索引

PUT test

# 删除索引

DELETE test

# 根据ID插入或者更新数据

PUT test/_doc/1

{

&quot;user&quot;:&quot;张三&quot;,

&quot;age&quot;:14

}

# 插入数据 ID自动设置

POST test/_doc

{

&quot;user&quot;:&quot;张三&quot;,

&quot;age&quot;:14

}

# 查询所有

GET test/_search

# 根据ID查询

GET test/_doc/1

# 带参查询

GET test/_search?q=user=&quot;张三&quot;

# 超时查询

GET test/_search?timeout=1ms

# 分页排序查询

GET test/_search?from=0&amp;size=2&amp;sort=user.keyword:desc
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>DSL</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>POST test/_doc

{

&quot;user&quot;:&quot;李三&quot;,

&quot;age&quot;:18

}

# 分词查找 分成李和三去找

GET test/_search

{

&quot;query&quot;:{

&quot;match&quot;: {

&quot;user&quot;: &quot;李三&quot;

}

}

}

# 关键词查找

GET test/_search

{

&quot;query&quot;:{

&quot;term&quot;: {

&quot;user&quot;: &quot;李&quot;

}

}

}

POST test/_doc

{

&quot;user&quot;:&quot;李三&quot;,

&quot;age&quot;:18,

&quot;body&quot;:14

}

# 多条件查询

GET test/_search

{

&quot;query&quot;:{

&quot;multi_match&quot;: {

&quot;query&quot;: &quot;14&quot;,

&quot;fields&quot;: [&quot;body&quot;,&quot;age&quot;]

}

}

}

# 排序

GET test/_search

{

&quot;sort&quot;:[

{

&quot;user.keyword&quot;:{

&quot;order&quot;:&quot;desc&quot;

} }

]

}

# 指定返回字段

GET test/_search

{

&quot;_source&quot;:[

&quot;user&quot;

]

}

# 分页查询

GET test/_search

{

&quot;from&quot;:0,

&quot;size&quot;:1

}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>filter 查询，比DSL快</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>
# filter:过滤器不计算相关分数。

# must:必须有

# should: 可有可无

# must_not:必须无

# minimum_should_match 表示查询时should的占比

GET test/_search

{

&quot;query&quot;:{

&quot;bool&quot;: {

&quot;must&quot;: [

{&quot;term&quot;: {

&quot;user&quot;: {

&quot;value&quot;: &quot;三&quot;

}

}}

],

&quot;must_not&quot;: [

{&quot;match&quot;: {

&quot;user&quot;: &quot;李&quot;

}}

],

&quot;filter&quot;: [

{&quot;term&quot;: {

&quot;age&quot;: &quot;14&quot;

}}

]

}

}

}

# 多条件查询

GET /ks-logstash-log-2023.06.24/_search

{

&quot;query&quot;:{

&quot;bool&quot;: {

&quot;must&quot;: [

{ &quot;match&quot;: { &quot;kubernetes.container_name&quot;: &quot;prod-supply-sp-api&quot;}},

{ &quot;match&quot;: { &quot;log&quot;: &quot;Exception&quot;} }

]

}

},

&quot;_source&quot;:[&quot;log&quot;],

&quot;sort&quot;:[

{&quot;time&quot;:{&quot;order&quot;:&quot;desc&quot;}}

]

}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>高亮查询</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>GET test/_search

{

&quot;query&quot;:{

&quot;match&quot;: {

&quot;user&quot;: &quot;李&quot;

}

},

&quot;highlight&quot;:{

&quot;fields&quot;:{

&quot;user&quot;:{

&quot;pre_tags&quot;:[&quot;&lt;h1&gt;&quot;],

&quot;post_tags&quot;:[&quot;&lt;/h1&gt;&quot;]

}

}

}

}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>游标查询，在规定时间内，只要拿着第一次放回的ID一直请求就会一直向下查询</p><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>GET test/_search?scroll=10m

{

&quot;size&quot;:1

}

# 可以一直请求，直到所有数据返回

GET _search/scroll

{

&quot;scroll&quot;:&quot;1m&quot;,

&quot;scroll_id&quot;:&quot;FGluY2x1ZGVfY29udGV4dF91dWlkDXF1ZXJ5QW5kRmV0Y2gBFm1meFdWMmZGUUstemVRcWE5c043T1EAAAAAAACDXBZZeG1VS3JvcFNnbWhaTVc0a3BseVh3&quot;

}
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>倒排索引</p><p>我们通常的搜索都是从文章中去找关键词，一个一个文章的去找，这样的方式显然是效率低的；</p><img src="`+l+'" width="300px"><p>所以倒排索引就出现了，用关键词作为索引去关联个个文章，那么查找的时候只要找到这些关键词，就能找到对应的个篇文章了。</p><img src="'+d+'" width="300px"></li></ol>',2),a=[u];function r(c,m){return n(),e("div",null,a)}const t=i(v,[["render",r],["__file","elasticsearch.html.vue"]]);export{t as default};
