import{_ as n,o as s,c as a,e as p}from"./app-9c8f6567.js";const t={},e=p(`<h1 id="typescript" tabindex="-1"><a class="header-anchor" href="#typescript" aria-hidden="true">#</a> typeScript</h1><p>ts只是对js的一种封装，对js的缺点进行弥补（变量和函数没有类型限制）;<br> ts中也可以直接写js代码;<br> ts需要编译（tsc）成js才能运行;</p><ul><li><p>类型</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">let</span> a<span class="token operator">:</span> <span class="token builtin">number</span> <span class="token operator">=</span> <span class="token number">123</span><span class="token punctuation">;</span>
<span class="token keyword">let</span> a<span class="token operator">:</span> <span class="token builtin">string</span> <span class="token operator">=</span> <span class="token string">&#39;123&#39;</span><span class="token punctuation">;</span>
<span class="token keyword">let</span> a<span class="token operator">:</span> <span class="token builtin">boolean</span> <span class="token operator">=</span> <span class="token boolean">true</span><span class="token punctuation">;</span>
<span class="token keyword">let</span> a<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">[</span><span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">,</span><span class="token number">2</span><span class="token punctuation">]</span><span class="token punctuation">;</span>
<span class="token keyword">let</span> x<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token builtin">string</span><span class="token punctuation">,</span> <span class="token builtin">number</span><span class="token punctuation">]</span><span class="token punctuation">;</span>

<span class="token comment">// 元组</span>
x <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token string">&#39;Runoob&#39;</span><span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">]</span><span class="token punctuation">;</span>    <span class="token comment">// 运行正常</span>
x <span class="token operator">=</span> <span class="token punctuation">[</span><span class="token number">1</span><span class="token punctuation">,</span> <span class="token string">&#39;Runoob&#39;</span><span class="token punctuation">]</span><span class="token punctuation">;</span>    <span class="token comment">// 报错</span>
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>x<span class="token punctuation">[</span><span class="token number">0</span><span class="token punctuation">]</span><span class="token punctuation">)</span><span class="token punctuation">;</span>    <span class="token comment">// 输出 Runoob</span>

<span class="token comment">// 枚举</span>
<span class="token keyword">enum</span> Color <span class="token punctuation">{</span>Red<span class="token punctuation">,</span> Green<span class="token punctuation">,</span> Blue<span class="token punctuation">}</span><span class="token punctuation">;</span>
<span class="token keyword">let</span> c<span class="token operator">:</span> Color <span class="token operator">=</span> Color<span class="token punctuation">.</span>Blue<span class="token punctuation">;</span>
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>c<span class="token punctuation">)</span><span class="token punctuation">;</span>    <span class="token comment">// 输出 2</span>

<span class="token comment">// 用于标识方法返回值的类型，表示该方法没有返回值。</span>
<span class="token keyword">function</span> <span class="token function">hello</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token operator">:</span> <span class="token keyword">void</span> <span class="token punctuation">{</span>
    <span class="token function">alert</span><span class="token punctuation">(</span><span class="token string">&quot;Hello Runoob&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token comment">// null 表示对象值缺失</span>
<span class="token comment">// undefined 用于初始化变量为一个未定义的值</span>
<span class="token comment">// never  是其它类型（包括 null 和 undefined）的子类型，代表从不会出现的值。</span>

<span class="token comment">// 联合类型</span>
<span class="token keyword">var</span> val<span class="token operator">:</span><span class="token builtin">string</span><span class="token operator">|</span><span class="token builtin">number</span> 
val <span class="token operator">=</span> <span class="token number">12</span> 
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;数字为 &quot;</span><span class="token operator">+</span> val<span class="token punctuation">)</span> 
val <span class="token operator">=</span> <span class="token string">&quot;Runoob&quot;</span> 
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;字符串为 &quot;</span> <span class="token operator">+</span> val<span class="token punctuation">)</span>

<span class="token comment">// 任何，不限定类型</span>
<span class="token keyword">let</span> z<span class="token operator">:</span><span class="token builtin">any</span>

<span class="token comment">// 未知类型</span>
<span class="token keyword">let</span> z<span class="token operator">:</span><span class="token builtin">unknown</span>

<span class="token comment">// 类型断言:类型断言可以用来手动指定一个值的类型，即允许变量从一种类型更改为另一种类型。</span>
<span class="token keyword">let</span> str <span class="token operator">=</span> <span class="token string">&#39;1&#39;</span> 
<span class="token comment">// 写法1</span>
<span class="token keyword">let</span> str2<span class="token operator">:</span><span class="token builtin">number</span> <span class="token operator">=</span>  str <span class="token keyword">as</span> <span class="token builtin">number</span>  <span class="token comment">//str、str2 是 string 类型</span>
<span class="token comment">// 写法2 类似java的List&lt;String&gt;,泛型 </span>
<span class="token keyword">let</span> str2<span class="token operator">:</span><span class="token builtin">number</span> <span class="token operator">=</span>  <span class="token operator">&lt;</span><span class="token builtin">number</span><span class="token operator">&gt;</span> str  

<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>str2<span class="token punctuation">)</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>作用域</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">var</span> global_num <span class="token operator">=</span> <span class="token number">12</span>          <span class="token comment">// 全局变量</span>
<span class="token keyword">class</span> <span class="token class-name">Numbers</span> <span class="token punctuation">{</span> 
num_val <span class="token operator">=</span> <span class="token number">13</span><span class="token punctuation">;</span>             <span class="token comment">// 实例变量</span>
<span class="token keyword">static</span> sval <span class="token operator">=</span> <span class="token number">10</span><span class="token punctuation">;</span>         <span class="token comment">// 静态变量</span>

<span class="token function">storeNum</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token operator">:</span><span class="token keyword">void</span> <span class="token punctuation">{</span> 
    <span class="token keyword">var</span> local_num <span class="token operator">=</span> <span class="token number">14</span><span class="token punctuation">;</span>    <span class="token comment">// 局部变量</span>
<span class="token punctuation">}</span> 
<span class="token punctuation">}</span> 
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>以上代码使用 tsc 命令编译为 JavaScript 代码为：</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">var</span> global_num <span class="token operator">=</span> <span class="token number">12</span><span class="token punctuation">;</span> <span class="token comment">// 全局变量</span>
<span class="token keyword">var</span> Numbers <span class="token operator">=</span> <span class="token doc-comment comment">/** <span class="token keyword">@class</span> */</span> <span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">function</span> <span class="token function">Numbers</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">this</span><span class="token punctuation">.</span>num_val <span class="token operator">=</span> <span class="token number">13</span><span class="token punctuation">;</span> <span class="token comment">// 实例变量</span>
    <span class="token punctuation">}</span>
    <span class="token class-name">Numbers</span><span class="token punctuation">.</span>prototype<span class="token punctuation">.</span><span class="token function-variable function">storeNum</span> <span class="token operator">=</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">var</span> local_num <span class="token operator">=</span> <span class="token number">14</span><span class="token punctuation">;</span> <span class="token comment">// 局部变量</span>
    <span class="token punctuation">}</span><span class="token punctuation">;</span>
    Numbers<span class="token punctuation">.</span>sval <span class="token operator">=</span> <span class="token number">10</span><span class="token punctuation">;</span> <span class="token comment">// 静态变量</span>
    <span class="token keyword">return</span> Numbers<span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>函数</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">function</span> <span class="token function">add</span><span class="token punctuation">(</span>x<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">,</span> y<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">)</span><span class="token operator">:</span> <span class="token builtin">number</span> <span class="token punctuation">{</span>
<span class="token keyword">return</span> x <span class="token operator">+</span> y<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token function">add</span><span class="token punctuation">(</span><span class="token number">1</span><span class="token punctuation">,</span><span class="token number">2</span><span class="token punctuation">)</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>接口<br> 也是为了限定类型</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code>    <span class="token comment">// 接口，也相当于一种类型限定</span>
<span class="token keyword">interface</span> <span class="token class-name">IPerson</span> <span class="token punctuation">{</span> 
firstName<span class="token operator">:</span><span class="token builtin">string</span><span class="token punctuation">,</span> 
lastName<span class="token operator">:</span><span class="token builtin">string</span><span class="token punctuation">,</span> 
<span class="token function-variable function">sayHi</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token operator">=&gt;</span><span class="token builtin">string</span> 
<span class="token punctuation">}</span> 

<span class="token keyword">var</span> customer<span class="token operator">:</span>IPerson <span class="token operator">=</span> <span class="token punctuation">{</span> 
    firstName<span class="token operator">:</span><span class="token string">&quot;Tom&quot;</span><span class="token punctuation">,</span>
    lastName<span class="token operator">:</span><span class="token string">&quot;Hanks&quot;</span><span class="token punctuation">,</span> 
    sayHi<span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token operator">:</span><span class="token builtin">string</span> <span class="token operator">=&gt;</span><span class="token punctuation">{</span><span class="token keyword">return</span> <span class="token string">&quot;Hi there&quot;</span><span class="token punctuation">}</span> 
<span class="token punctuation">}</span> 
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>类 相当于定义类型</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">class</span> <span class="token class-name">Car</span> <span class="token punctuation">{</span> 
    <span class="token comment">// 字段</span>
    engine<span class="token operator">:</span><span class="token builtin">string</span><span class="token punctuation">;</span> 
    
    <span class="token comment">// 构造函数</span>
    <span class="token function">constructor</span><span class="token punctuation">(</span>engine<span class="token operator">:</span><span class="token builtin">string</span><span class="token punctuation">)</span> <span class="token punctuation">{</span> 
        <span class="token keyword">this</span><span class="token punctuation">.</span>engine <span class="token operator">=</span> engine 
    <span class="token punctuation">}</span>  
    
    <span class="token comment">// 方法</span>
    <span class="token function">disp</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token operator">:</span><span class="token keyword">void</span> <span class="token punctuation">{</span> 
        <span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;函数中显示发动机型号  :   &quot;</span><span class="token operator">+</span>thisengine<span class="token punctuation">)</span> 
    <span class="token punctuation">}</span> 
<span class="token punctuation">}</span> 

<span class="token comment">// 创建一个对象</span>
<span class="token keyword">var</span> obj <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Car</span><span class="token punctuation">(</span><span class="token string">&quot;XXSY1&quot;</span><span class="token punctuation">)</span>

<span class="token comment">// 访问字段</span>
<span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;读取发动机型号 :  &quot;</span><span class="token operator">+</span>obj<span class="token punctuation">.</span>engine<span class="token punctuation">)</span>  

<span class="token comment">// 访问方法</span>
obj<span class="token punctuation">.</span><span class="token function">disp</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>编译以上代码，得到以下 JavaScript 代码：</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">var</span> Car <span class="token operator">=</span>  <span class="token punctuation">(</span><span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token comment">// 构造函数</span>
    <span class="token keyword">function</span> <span class="token function">Car</span><span class="token punctuation">(</span><span class="token parameter">engine</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token keyword">this</span><span class="token punctuation">.</span>engine <span class="token operator">=</span> engine<span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
    <span class="token comment">// 方法</span>
    <span class="token class-name">Car</span><span class="token punctuation">.</span>prototype<span class="token punctuation">.</span><span class="token function-variable function">disp</span> <span class="token operator">=</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;函数中显示发动机型号  :   &quot;</span> <span class="token operator">+</span> <span class="token keyword">this</span><span class="token punctuation">.</span>engine<span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span><span class="token punctuation">;</span>
    <span class="token keyword">return</span> Car<span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">// 创建一个对象</span>
<span class="token keyword">var</span> obj <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Car</span><span class="token punctuation">(</span><span class="token string">&quot;XXSY1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">// 访问字段</span>
console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;读取发动机型号 :  &quot;</span> <span class="token operator">+</span> obj<span class="token punctuation">.</span>engine<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">// 访问方法</span>
obj<span class="token punctuation">.</span><span class="token function">disp</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>对象<br> 类是限定成员类型，对象是直接给的成员具体值</p><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code><span class="token keyword">var</span> sites <span class="token operator">=</span> <span class="token punctuation">{</span>
site1<span class="token operator">:</span> <span class="token string">&quot;Runoob&quot;</span><span class="token punctuation">,</span>
site2<span class="token operator">:</span> <span class="token string">&quot;Google&quot;</span><span class="token punctuation">,</span>
<span class="token function-variable function">sayHello</span><span class="token operator">:</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span> <span class="token punctuation">}</span> <span class="token comment">// 类型模板</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>
sites<span class="token punctuation">.</span><span class="token function-variable function">sayHello</span> <span class="token operator">=</span> <span class="token keyword">function</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token builtin">console</span><span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span><span class="token string">&quot;hello &quot;</span> <span class="token operator">+</span> sites<span class="token punctuation">.</span>site1<span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span>
sites<span class="token punctuation">.</span><span class="token function">sayHello</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div></li></ul>`,3),o=[e];function c(l,i){return s(),a("div",null,o)}const r=n(t,[["render",c],["__file","type-scrip.html.vue"]]);export{r as default};
