import{_ as n,o as s,c as a,e}from"./app-dc7bc98b.js";const t="/lzy6666666/images/thread-pool.webp",c={},p=e(`<h1 id="线程池" tabindex="-1"><a class="header-anchor" href="#线程池" aria-hidden="true">#</a> 线程池</h1><ol><li>任务：创建出来的线程，还没有start, 交给线程池管理。</li><li>提交任务： 交给线程池管理（放到池子的集合里面）。</li><li>线程池：控制启动的线程数量，队列，最大数量等等。。。。。</li><li>最大线程数量：当任务队列和核心线程都饱和的时候，可创建的最大数量。</li></ol><p>线程池：保存创建好的线程并复用。<br> 代码运行完，main不会退出，需要调用shutdown才会退出。</p><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">PoolTest</span> <span class="token punctuation">{</span>
    <span class="token comment">// https://blog.csdn.net/u013541140/article/details/95225769</span>
    <span class="token keyword">public</span> <span class="token keyword">static</span> <span class="token keyword">void</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token class-name">String</span><span class="token punctuation">[</span><span class="token punctuation">]</span> args<span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token comment">// ArrayBlockingQueue</span>
        <span class="token comment">// LinkedBlockingQueue</span>
        <span class="token comment">// PriorityBlockingQueue</span>
        <span class="token comment">// DelayQueue</span>
        <span class="token comment">// SynchronousQueue</span>
        <span class="token comment">// LinkedBlockingDeque</span>
        <span class="token comment">// LinkedTransferQueue</span>
        <span class="token comment">// 线程队列</span>
        <span class="token class-name">BlockingQueue</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">Runnable</span><span class="token punctuation">&gt;</span></span> queue <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">ArrayBlockingQueue</span><span class="token punctuation">(</span><span class="token number">3</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token comment">// 线程创建工厂</span>
        <span class="token class-name">ThreadFactory</span> threadFactory <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">ThreadFactory</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
            <span class="token annotation punctuation">@Override</span>
            <span class="token keyword">public</span> <span class="token class-name">Thread</span> <span class="token function">newThread</span><span class="token punctuation">(</span><span class="token class-name">Runnable</span> r<span class="token punctuation">)</span> <span class="token punctuation">{</span>
                <span class="token keyword">return</span> <span class="token keyword">new</span> <span class="token class-name">Thread</span><span class="token punctuation">(</span>r<span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">;</span>
        <span class="token comment">// 拒绝策略RejectedExecutionHandler</span>
        <span class="token comment">// AbortPolicy（默认）：丢弃任务并抛出 RejectedExecutionException 异常。</span>
        <span class="token comment">// CallerRunsPolicy：由调用线程处理该任务。</span>
        <span class="token comment">// DiscardPolicy：丢弃任务，但是不抛出异常。可以配合这种模式进行自定义的处理方式。</span>
        <span class="token comment">// DiscardOldestPolicy：丢弃队列最早的未处理任务，然后重新尝试执行任务。</span>
        <span class="token class-name">RejectedExecutionHandler</span> handler <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">ThreadPoolExecutor<span class="token punctuation">.</span>CallerRunsPolicy</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token class-name">ThreadPoolExecutor</span> threadPoolExecutor <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">ThreadPoolExecutor</span><span class="token punctuation">(</span>
                <span class="token number">2</span><span class="token punctuation">,</span> <span class="token comment">// 核心线程数</span>
                <span class="token number">2</span><span class="token punctuation">,</span> <span class="token comment">// 最大线程数</span>
                <span class="token number">1000</span><span class="token punctuation">,</span> <span class="token comment">// 最大空闲时间</span>
                <span class="token class-name">TimeUnit</span><span class="token punctuation">.</span><span class="token constant">MILLISECONDS</span><span class="token punctuation">,</span> <span class="token comment">// 空闲时间单位</span>
                queue<span class="token punctuation">,</span> <span class="token comment">// 任务队列</span>
                threadFactory<span class="token punctuation">,</span> <span class="token comment">// 线程工厂</span>
                handler <span class="token comment">// 饱和机制</span>
                <span class="token punctuation">)</span><span class="token punctuation">;</span>
        threadPoolExecutor<span class="token punctuation">.</span><span class="token function">execute</span><span class="token punctuation">(</span><span class="token keyword">new</span> <span class="token class-name">Runnable</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
            <span class="token annotation punctuation">@Override</span>
            <span class="token keyword">public</span> <span class="token keyword">void</span> <span class="token function">run</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
                <span class="token class-name">System</span><span class="token punctuation">.</span>out<span class="token punctuation">.</span><span class="token function">println</span><span class="token punctuation">(</span><span class="token string">&quot;线程体1&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        
        <span class="token comment">// 已经定义好的功能线程池：可能引起OOM，已经不建议用</span>
        <span class="token comment">//定长线程池（FixedThreadPool）</span>
        <span class="token comment">//定时线程池（ScheduledThreadPool ）</span>
        <span class="token comment">//可缓存线程池（CachedThreadPool）</span>
        <span class="token comment">//单线程化线程池（SingleThreadExecutor）</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><p>线程池状态<br> 线程池有5种状态：Running、ShutDown、Stop、Tidying、Terminated<br><img src="`+t+'"></p></li><li><p>线程池中 submit()和 execute() 方法有什么区别？<br> 接收的参数不一样<br> submit有返回值，而execute没有<br> submit方便Exception处理</p></li></ul>',5),o=[p];function l(i,u){return s(),a("div",null,o)}const r=n(c,[["render",l],["__file","线程池.html.vue"]]);export{r as default};
