import{_ as t,r as d,o as c,c as v,a as n,b as e,d as i,w as r,e as l}from"./app-d6c071ec.js";const o={},u=n("h1",{id:"ora2pg",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#ora2pg","aria-hidden":"true"},"#"),e(" Ora2Pg")],-1),b=n("pre",null,[n("code",null,`oracle 数据迁移 postgreSql免费开源工具。                                     
`)],-1),m=n("ul",null,[n("li",null,"window版本")],-1),p={href:"https://strawberryperl.com/",target:"_blank",rel:"noopener noreferrer"},g=n("br",null,null,-1),h=l(`<ul><li>配置perl<br> perl Makefile.PL<br> dmake &amp;&amp; dmake install<br> gmake &amp;&amp; gmake install</li></ul><div class="language-cmd line-numbers-mode" data-ext="cmd"><pre class="language-cmd"><code>    F:\\itsoft\\ora2pg-23.2\\ora2pg-23.2&gt;perl Makefile.PL
    Checking if your kit is complete...
    Looks good
    Invalid LICENSE value &#39;GPLv3&#39; ignored
    Generating a gmake-style Makefile
    Writing Makefile for Ora2Pg
    Invalid LICENSE value &#39;GPLv3&#39; ignored
    Writing MYMETA.yml and MYMETA.json

    Done...
    ------------------------------------------------------------------------------
    Please read documentation at http://ora2pg.darold.net/ before asking for help
    ------------------------------------------------------------------------------
    Now type: dmake &amp;&amp; dmake install

    F:\\itsoft\\ora2pg-23.2\\ora2pg-23.2&gt;dmake &amp;&amp; dmake install
    #############################################################
    ###                                                       ###
    ###                     DMAKE WARNING                     ###
    ###                                                       ###
    ###  Do not use dmake.exe utility as it is no longer      ###
    ###  part of Strawberry Perl, use gmake.exe instead!      ###
    ###                                                       ###
    ###  If you have troubles with CPAN client delete:        ###
    ###  %USERPROFILE%\\AppData\\Local\\.cpan\\CPAN\\MyConfig.pm   ###
    ###                                                       ###
    #############################################################

    F:\\itsoft\\ora2pg-23.2\\ora2pg-23.2&gt;gmake &amp;&amp; gmake install
    cp lib/Ora2Pg/PLSQL.pm blib\\lib\\Ora2Pg\\PLSQL.pm
    cp lib/Ora2Pg/Oracle.pm blib\\lib\\Ora2Pg\\Oracle.pm
    cp lib/Ora2Pg/GEOM.pm blib\\lib\\Ora2Pg\\GEOM.pm
    cp lib/Ora2Pg/MySQL.pm blib\\lib\\Ora2Pg\\MySQL.pm
    cp lib/Ora2Pg.pm blib\\lib\\Ora2Pg.pm
    &quot;F:\\itsoft\\perl\\perl\\bin\\perl.exe&quot; -MExtUtils::Command -e cp -- scripts/ora2pg blib\\script\\ora2pg
    pl2bat.bat blib\\script\\ora2pg
    &quot;F:\\itsoft\\perl\\perl\\bin\\perl.exe&quot; -MExtUtils::Command -e cp -- scripts/ora2pg_scanner blib\\script\\ora2pg_scanner
    pl2bat.bat blib\\script\\ora2pg_scanner
    Installing F:\\itsoft\\perl\\perl\\site\\lib\\Ora2Pg.pm
    Installing F:\\itsoft\\perl\\perl\\site\\lib\\Ora2Pg\\GEOM.pm
    Installing F:\\itsoft\\perl\\perl\\site\\lib\\Ora2Pg\\MySQL.pm
    Installing F:\\itsoft\\perl\\perl\\site\\lib\\Ora2Pg\\Oracle.pm
    Installing F:\\itsoft\\perl\\perl\\site\\lib\\Ora2Pg\\PLSQL.pm
    Installing F:\\itsoft\\perl\\perl\\site\\bin\\ora2pg
    Installing F:\\itsoft\\perl\\perl\\site\\bin\\ora2pg.bat
    Installing F:\\itsoft\\perl\\perl\\site\\bin\\ora2pg_scanner
    Installing F:\\itsoft\\perl\\perl\\site\\bin\\ora2pg_scanner.bat
    &quot;Installing default configuration file (ora2pg.conf.dist) to C:\\ora2pg&quot;
    Appending installation info to F:\\itsoft\\perl\\perl\\lib/perllocal.pod
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,2),f=n("br",null,null,-1),_=n("br",null,null,-1),O=n("br",null,null,-1),E=n("br",null,null,-1),D=n("br",null,null,-1),L=n("br",null,null,-1),P=l(`<div class="language-cmd line-numbers-mode" data-ext="cmd"><pre class="language-cmd"><code>    F:\\itsoft\\ora2pg-23.2\\ora2pg-23.2&gt;cpan
    Loading internal logger. Log::Log4perl recommended for better logging
    Unable to get Terminal Size. The Win32 GetConsoleScreenBufferInfo call didn&#39;t work. The COLUMNS and LINES environment variables didn&#39;t work. at F:\\itsoft\\perl\\perl\\vendor\\lib/Term/ReadLine/readline.pm line 410.

    cpan shell -- CPAN exploration and modules installation (v2.28)
    Enter &#39;h&#39; for help.

    cpan&gt; get DBI
    Running get for module &#39;DBI&#39;
    Fetching with LWP:
    http://cpan.strawberryperl.com/authors/id/T/TI/TIMB/DBI-1.643.tar.gz
    Fetching with LWP:
    http://cpan.strawberryperl.com/authors/id/T/TI/TIMB/CHECKSUMS
    Checksum for F:\\itsoft\\perl\\cpan\\sources\\authors\\id\\T\\TI\\TIMB\\DBI-1.643.tar.gz ok

    cpan&gt; install DBI
    DBI is up to date (1.643).

    cpan&gt; get DBD::Oracle
    Running get for module &#39;DBD::Oracle&#39;
    Fetching with LWP:
    http://cpan.strawberryperl.com/authors/id/Z/ZA/ZARQUON/DBD-Oracle-1.83.tar.gz
    Fetching with LWP:
    http://cpan.strawberryperl.com/authors/id/Z/ZA/ZARQUON/CHECKSUMS
    Checksum for F:\\itsoft\\perl\\cpan\\sources\\authors\\id\\Z\\ZA\\ZARQUON\\DBD-Oracle-1.83.tar.gz ok
    Scanning cache F:\\itsoft\\perl\\cpan\\build for sizes
    DONE

    cpan&gt; install DBD::Oracle
    Running install for module &#39;DBD::Oracle&#39;
    ZARQUON/DBD-Oracle-1.83.tar.gz
    Has already been unwrapped into directory F:\\itsoft\\perl\\cpan\\build\\DBD-Oracle-1.83-0
    Configuring Z/ZA/ZARQUON/DBD-Oracle-1.83.tar.gz with Makefile.PL
    Using DBI 1.643 (for perl 5.032001 on MSWin32-x64-multi-thread) installed in F:/itsoft/perl/perl/vendor/lib/auto/DBI/
    Configuring DBD::Oracle for perl 5.032001 on MSWin32 (MSWin32-x64-multi-thread)

    If you encounter any problem, a collection of troubleshooting
    guides are available under lib/DBD/Oracle/Troubleshooting.
    &#39;DBD::Oracle::Troubleshooting&#39; is the general troubleshooting
    guide, while platform-specific troubleshooting hints
    live in their labelled sub-document (e.g., Win32
    hints are gathered in &#39;lib/DBD/Oracle/Troubleshooting/Win32.pod&#39;).

    Trying to find an ORACLE_HOME
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,1),C={href:"https://sourceforge.net/projects/ora2pg/",target:"_blank",rel:"noopener noreferrer"},I=l(`<div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code># 设置Oracle主目录：Oracle的安装目录
ORACLE_HOME	F:\\itsoft\\instantclient_19_19

# 设置Oracle数据库连接（数据源、用户、密码）连接远程的，需要配置远程数据库ip和端口
ORACLE_DSN	dbi:Oracle:host=127.0.0.1;sid=ORCL;port=1521
ORACLE_USER	system
ORACLE_PWD	123456

# 需要迁移什么内容，就配置什么内容
TYPE TABLE,VIEW,SEQUENCE,TRIGGER,FUNCTION,PROCEDURE

# 导出SQL目录
OUTPUT F:\\ora2pg_output

# 这里配置你自己的Oracle数据库 不配置的情况下默认导出所有表
SCHEMA XXXX
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,1),S=l(`<div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>    # 设置Oracle主目录：Oracle的安装目录
    ORACLE_HOME	F:\\itsoft\\instantclient_19_19

    # 设置Oracle数据库连接（数据源、用户、密码）连接远程的，需要配置远程数据库ip和端口
    ORACLE_DSN	dbi:Oracle:host=192.168.14.200;sid=LCTEST;port=1521
    ORACLE_USER	ods
    ORACLE_PWD	Ow2wCF4gvsv

    # 需要迁移什么内容，就配置什么内容
    TYPE TABLE,VIEW,SEQUENCE,TRIGGER,FUNCTION,PROCEDURE

    # 导出SQL目录
    OUTPUT F:\\ora2pg_output

    # 这里配置你自己的Oracle数据库 不配置的情况下默认导出所有表 （选配）
    SCHEMA ods

    # 需要导出的具体表（选配），不配置默认所有
    ALLOW		SP_LEDGER_DETAIL
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>如果需要直接导入到pg数据库，则需要加如下配置</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>PG_DSN        dbi:Pg:dbname=fsj;host=192.168.1.3;port=5432

PG_USER      fsj

PG_PWD       fsjpwd

PG_SCHEMA  fsj
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,3),A=n("li",null,[n("p",null,"开始导数据")],-1),B=l(`<div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>    # 加-d可以看到详细的执行过程，方便调试
    ora2pg -c ora2pg_dist.conf -d
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div>`,1),R={href:"https://blog.csdn.net/fsj/article/details/112801114",target:"_blank",rel:"noopener noreferrer"};function k(x,T){const s=d("ExternalLinkIcon"),a=d("font");return c(),v("div",null,[u,b,m,n("ol",null,[n("li",null,[n("p",null,[n("a",p,[e("安装perl"),i(s)]),g,e(" strawberry-perl-5.32.1.1-64bit.msi ，下载完直接安装即可；")]),h,n("ul",null,[n("li",null,[e("使用perl 军火库安装 DBI和DBD"),f,e(" cpan"),_,e(" get DBI"),O,e(" install DBI"),E,e(" get DBD::Oracle"),D,e(" install DBD::Oracle"),L,e(" 因为我本地没有安装Oracle所以报错了，"),i(a,{color:"orange"},{default:r(()=>[e("要使用必须在本地安装Oracle或者 Oracle Client ")]),_:1})])]),P]),n("li",null,[n("p",null,[n("a",C,[e("下载ora2pg"),i(s)])]),n("ul",null,[n("li",null,[e("配置安装目录下的ora2pg.conf.dist "),n("ul",null,[n("li",null,[i(a,{color:"orange"},{default:r(()=>[e("操作本地ORACLE")]),_:1})])])])]),I,n("ul",null,[n("li",null,[i(a,{color:"orange"},{default:r(()=>[e("操作远程的ORACLE")]),_:1})])]),S]),A]),B,n("p",null,[n("a",R,[e("参考资料"),i(s)])])])}const M=t(o,[["render",k],["__file","ora2pg.html.vue"]]);export{M as default};
