import{_ as n,o as s,c as a,e}from"./app-9c8f6567.js";const p="/lzy6666666/images/spi.png",t={},c=e(`<h1 id="spi" tabindex="-1"><a class="header-anchor" href="#spi" aria-hidden="true">#</a> SPI</h1><p>使用SPI比较简单，只需要按照以下几个步骤</p><ol><li>创建一个接口</li><li>在jar包的 META-INF/services 目录下创建一个以&quot;接口全限定名&quot;为命名的文件，内容为实现类的全限定名（SPI的实现类必须带一个无参构造方法）</li><li>以接口的全路径做为名称，创建文件（注意不要带文件格式后缀）</li><li>在文件中写入实现类的全路径</li><li>在代码中调用ServiceLoader.load(接口.class)获取迭代器</li><li>遍历迭代器即可获取对应实现类的实例，通过扫描META-INF/services目录下的配置文件找到实现类的全限定名，把类加载到JVM</li></ol><ul><li>接口</li></ul><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">package</span> <span class="token namespace">com<span class="token punctuation">.</span>lzy<span class="token punctuation">.</span>springbootstart<span class="token punctuation">.</span>spi</span><span class="token punctuation">;</span>

<span class="token doc-comment comment">/**
 * <span class="token keyword">@author</span> Tim
 * <span class="token keyword">@description</span>: spi测试
 * <span class="token keyword">@date</span> 2022/4/14 13:53
 */</span>
<span class="token keyword">public</span> <span class="token keyword">interface</span> <span class="token class-name">SpiTest</span> <span class="token punctuation">{</span>
    <span class="token keyword">void</span> <span class="token function">test</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>实现类1</li></ul><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">package</span> <span class="token namespace">com<span class="token punctuation">.</span>lzy<span class="token punctuation">.</span>springbootstart<span class="token punctuation">.</span>spi</span><span class="token punctuation">;</span>

<span class="token doc-comment comment">/**
 * <span class="token keyword">@author</span> Tim
 * <span class="token keyword">@description</span>:
 * <span class="token keyword">@date</span> 2022/4/14 13:53
 */</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">SpiTestOneImpl</span> <span class="token keyword">implements</span> <span class="token class-name">SpiTest</span> <span class="token punctuation">{</span>
    <span class="token annotation punctuation">@Override</span>
    <span class="token keyword">public</span> <span class="token keyword">void</span> <span class="token function">test</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token class-name">System</span><span class="token punctuation">.</span>out<span class="token punctuation">.</span><span class="token function">println</span><span class="token punctuation">(</span><span class="token string">&quot;使用实现one&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>实现类2</li></ul><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">package</span> <span class="token namespace">com<span class="token punctuation">.</span>lzy<span class="token punctuation">.</span>springbootstart<span class="token punctuation">.</span>spi</span><span class="token punctuation">;</span>

<span class="token doc-comment comment">/**
 * <span class="token keyword">@author</span> Tim
 * <span class="token keyword">@description</span>:
 * <span class="token keyword">@date</span> 2022/4/14 13:53
 */</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">SpiTestTwoImpl</span> <span class="token keyword">implements</span> <span class="token class-name">SpiTest</span> <span class="token punctuation">{</span>
    <span class="token annotation punctuation">@Override</span>
    <span class="token keyword">public</span> <span class="token keyword">void</span> <span class="token function">test</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token class-name">System</span><span class="token punctuation">.</span>out<span class="token punctuation">.</span><span class="token function">println</span><span class="token punctuation">(</span><span class="token string">&quot;使用实现two&quot;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>测试</li></ul><div class="language-java line-numbers-mode" data-ext="java"><pre class="language-java"><code><span class="token keyword">import</span> <span class="token import"><span class="token namespace">java<span class="token punctuation">.</span>util<span class="token punctuation">.</span></span><span class="token class-name">ServiceLoader</span></span><span class="token punctuation">;</span>

<span class="token doc-comment comment">/**
 * <span class="token keyword">@author</span> Tim
 * <span class="token keyword">@description</span>: 测试Spi
 * <span class="token keyword">@date</span> 2022/4/14 13:58
 */</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">Test</span> <span class="token punctuation">{</span>
    <span class="token keyword">public</span> <span class="token keyword">static</span> <span class="token keyword">void</span> <span class="token function">main</span><span class="token punctuation">(</span><span class="token class-name">String</span><span class="token punctuation">[</span><span class="token punctuation">]</span> args<span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token class-name">ServiceLoader</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">SpiTest</span><span class="token punctuation">&gt;</span></span> loader <span class="token operator">=</span> <span class="token class-name">ServiceLoader</span><span class="token punctuation">.</span><span class="token function">load</span><span class="token punctuation">(</span><span class="token class-name">SpiTest</span><span class="token punctuation">.</span><span class="token keyword">class</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        loader<span class="token punctuation">.</span><span class="token function">forEach</span><span class="token punctuation">(</span>spiTest <span class="token operator">-&gt;</span> <span class="token punctuation">{</span>
            spiTest<span class="token punctuation">.</span><span class="token function">test</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>META-INF/services</li></ul><p><img src="`+p+'" alt="图片"></p>',13),i=[c];function l(o,u){return s(),a("div",null,i)}const k=n(t,[["render",l],["__file","SPI.html.vue"]]);export{k as default};
